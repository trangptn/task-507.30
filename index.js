//import thu vien express
const express = require('express');
const path = require('path');

const app= new express();

const port= 8000;

app.get("/",(req,res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/sample.06restAPI.order.pizza365.v2.0.html"))
})

app.listen(port, () => {
    console.log(`App  listening  on port ${port}`);
})